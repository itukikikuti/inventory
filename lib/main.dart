import "package:flutter/material.dart";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var icons = {
      "ドロイド君": Icons.android,
      "雪結晶": Icons.ac_unit,
      "時計": Icons.access_alarm,
      "？": Icons.account_circle,
      "飛行機": Icons.airplanemode_active,
      "!": Icons.announcement,
      "apps": Icons.apps,
      "build": Icons.build,
      "download": Icons.cloud_download,
      "folder": Icons.folder,
      "bluetooth": Icons.bluetooth,
      "brightness": Icons.brightness_5,
      "email": Icons.email,
      "voice": Icons.keyboard_voice,
      "refresh": Icons.refresh,
      "screen rotation": Icons.screen_rotation,
      "wifi": Icons.wifi,
    };
    return MaterialApp(
      title: "Inventory",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Invectory"),
        ),
        body: GridView.count(
          crossAxisCount: 3,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          padding: const EdgeInsets.all(10.0),
          children: List.generate(icons.length, (index) {
            return GridTile(
              child: OutlineButton(
                child: Stack(
                  children: [
                    Center(child: Icon(icons.values.elementAt(index), size: 50.0)),
                    Align(child: Text(icons.keys.elementAt(index)), alignment: Alignment.bottomCenter),
                  ]),
                onPressed: () {},
                borderSide: BorderSide(color: Colors.grey, width: 2.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
            );
          }),
        ),
      ),
    );
  }
}
